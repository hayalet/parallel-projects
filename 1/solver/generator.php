<?php

ob_start();

$baseName = pathinfo(basename(__FILE__), PATHINFO_FILENAME);

$configFile = (!empty($opts['config'])) ? $opts['config'] : $baseName . '.ini';
if (!file_exists($configFile)) {
	throw new ErrorException('Config file does not exist: ' . $configFile);
}

echo "Reading config" . PHP_EOL;
$config = parse_ini_file($configFile);

$optNames = array_map(function ($value) { return $value . '::';}, array_keys($config));
$opts = getopt('', $optNames);

foreach ($opts as $key => $value) {
	if ($value === false) {
		$value = true;
	}

	$config[$key] = str_replace(array('false', 'true'), array('0', '1'), $value);		
}

if (empty($config['input'])) {
	throw new InvalidArgumentException('Input file is not specified');
}

$inputFile = $config['input'];
$inputName = pathinfo(basename($inputFile), PATHINFO_FILENAME);

$resultFile = $inputName . '.dat';
$ganttFile = $inputName . '.gantt.tex';
$tableFile = $inputName . '.table.tex';

if (!$config['cache']) {
	echo "Clearing cache" . PHP_EOL;
	foreach (array($resultFile, $ganttFile, $tableFile) as $file) {
		if (file_exists($file)) {
			unlink($file);
		}
	}
}

$graphFile = $inputName . '.php';
if (!file_exists($graphFile)) {
	throw new ErrorException('Graph file does not exist: ' . $graphFile);
}

echo "Reading input file" . PHP_EOL;
$inputData = file_get_contents($inputFile);

$resultData = null;
if (!file_exists($resultFile)) {
	$command = $config['exec'] . ' -rxli ' . $config['xli'] . ' ' . $inputFile;
	echo "Executing solver using command:" . PHP_EOL;
	echo $command . PHP_EOL;
	$start = microtime(true);
	exec($command, $resultData);
	$stop = microtime(true);
	echo "Time elapsed: " . ($stop - $start) . "s" . PHP_EOL;
	$resultData = implode(PHP_EOL, $resultData);
	echo "Caching results to: " . $resultFile . PHP_EOL;
	file_put_contents($resultFile, $resultData);
}
else {
	echo "Loading results from cache" . PHP_EOL;
	$resultData = file_get_contents($resultFile);
}

if (empty($resultData)) {
	throw new ErrorException("Result data is empty. Clear cache by hand and try again");
}

$libFile = $config['pp'];
if (!file_exists($libFile)) {
	throw new ErrorException("Parallel processing library not found in: " . $libFile);
}

require_once $libFile;

$graph = new \ParallelProcessing\Graph();
$input = new \ParallelProcessing\InputParser($inputData);
$result = new \ParallelProcessing\ResultParser($resultData);
$ganttTex = new \ParallelProcessing\GanttTexGenerator($graph);
$tableTex = new \ParallelProcessing\TableTexGenerator(array_merge(
	$input->params(),
	$result->values($input->vars())
));

require_once $graphFile;

if ($config['debug']) {
	echo "Debuging graph displaying" . PHP_EOL;
	echo $graph->debug() . PHP_EOL;
	echo PHP_EOL;
}

echo "Generating gantt graph" . PHP_EOL;
if (file_exists($ganttFile) && !is_writeable($ganttFile)) {
	throw new ErrorException("File is not writeable: " . $ganttFile);
}
$ganttData = $ganttTex->generate();
echo "Saving to file: " . $ganttFile  . PHP_EOL;
file_put_contents($ganttFile, $ganttData);

echo "Generating values table" . PHP_EOL;
if (file_exists($tableFile) && !is_writeable($tableFile)) {
	throw new ErrorException("File is not writeable: " . $tableFile);
}
$tableData = $tableTex->generate();
echo "Saving to file: " . $tableFile  . PHP_EOL;
file_put_contents($tableFile, $tableData);

echo "Done";

if ($config['gantt']) {
	ob_clean();
	echo $ganttData;
}

if ($config['table']) {
	ob_clean();
	echo $tableData;
}

exit(0);