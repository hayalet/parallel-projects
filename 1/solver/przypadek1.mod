/**
 * Przypadek 1 - przypadek w którym procesor 1 inicjuje komunikacje
 * do procesorów 2 i 4, kolejność nie jest znana, następnie oba
 * sekwencyjnie komunikują się z procesorem 3 który odbiera z dwóch
 * źródeł i nie wysyła danych dalej
 *
 * Wersja z uwzględnieniem obliczeń podczas oczekiwania na
 * procesorach 2 i 4
 *
 * @author Mateusz Łuczak, Krystian Panek
 * @version 1.0
 */

/**
 * Model Systemu
 */

/* Parametry systemu */
param V := 96;
param M := 10000;

param A_0 := 5;
param A_1 := 5;
param A_2 := 3;
param A_3 := 1;
param A_4 := 5;

param C_01 := 1;
param C_12 := 1;
param C_14 := 3;
param C_23 := 2;
param C_43 := 8;

param S_01 := 0;
param S_12 := 0;
param S_14 := 0;
param S_23 := 0;
param S_43 := 0;

/* Deklaracje zmiennych */
var T >= 0;
var T_0 >= 0;
var T_1 >= 0;
var T_2 >= 0;
var T_3 >= 0;
var T_4 >= 0;
var t_RK12 >= 0;
var t_RK14 >= 0;
var t_RK23 >= 0;
var t_RK43 >= 0;
var a_0 integer >= 0;
var a_1 integer >= 0;
var a_2i integer >= 0;
var a_2ii integer >= 0;
var a_3i integer >= 0;
var a_3ii integer >= 0;
var a_4i integer >= 0;
var a_4ii integer >= 0;
var d_01 integer >= 0;
var d_12 integer >= 0;
var d_14 integer >= 0;
var d_23 integer >= 0;
var d_43 integer >= 0;
var x_01 integer >= 0 <= 1;
var x_12 integer >= 0 <= 1;
var x_14 integer >= 0 <= 1;
var x_23 integer >= 0 <= 1;
var x_43 integer >= 0 <= 1;
var y_124 integer >= 0 <= 1;
var y_324 integer >= 0 <= 1;

/**
 * Model matematyczny
 */

/* Funkcja celu */
minimize obj: +T;

/* Ograniczenia */

/*Procesor 0*/
R1: +T -T_0 >= 0;
R2: +T_0 -A_0*a_0 -C_01*d_01 -S_01*x_01 = 0;
R3: +a_0 +d_01 = V;
R4: -d_01 +V*x_01 >= 0;

/* Procesor 1 */
R5: +T -T_1 >= 0;
R6: +T_1 -A_1*a_1 -C_01*d_01 -S_01*x_01 -C_12*d_12 -S_12*x_12 -C_14*d_14 -S_14*x_14 = 0;
R7: +t_RK14 -C_01*d_01 -S_01*x_01 >= 0;
R8: +t_RK12 -C_01*d_01 -S_01*x_01 >= 0;
R9: +t_RK12 -C_01*d_01 -S_01*x_01 -C_14*d_14 -S_14*x_14 +y_124*M >= 0;
R10: +t_RK14 -C_01*d_01 -S_01*x_01 -C_12*d_12 -S_12*x_12 +M -y_124*M >= 0;
R11: -d_14 +V*x_14 >= 0;
R12: -d_12 +V*x_12 >= 0;
R13: +d_01 = +d_12 +d_14 +a_1;

/* Procesor 2 */
R14: +T -T_2 >= 0;
R15: +T_2 -A_2*a_2ii -t_RK23 -C_23*d_23 -S_23*x_23 >= 0;
R16: +t_RK23 -t_RK12 -C_12*d_12 -S_12*x_12 -A_2*a_2i >= 0;
R17: +d_12 = +d_23 +a_2i +a_2ii;
R18: -d_23 +V*x_23 >= 0;

/* Procesor 3 */
R19: +T -T_3 >= 0;
/* Gdy 2->3 jest wcześniej niż 3->4  (y_234 = 1) */
R20: +T_3 -A_3*a_3ii -t_RK43 -C_43*d_43 -S_43*x_43 +M -y_324*M >= 0;
R21: +t_RK43 -t_RK23 -C_23*d_23 -S_23*x_23 -A_3*a_3i +M -y_324*M >= 0;
R22: +d_23 +M -y_324*M >= +a_3i;
/* Gdy 4->3 jest wcześniej niż 2->3 (y_234 = 0) */
R23: +T_3 -A_3*a_3ii -t_RK23 -C_23*d_23 -S_23*x_23 +y_324*M >= 0;
R24: +t_RK23 -t_RK43 -C_43*d_43 -S_43*x_43 -A_3*a_3i +y_324*M >= 0;
R25: +d_43 +y_324*M >= +a_3i;
/* Dla obu */
R26: +d_23 +d_43 = +a_3i +a_3ii;

/* Procesor 4 */
R27: +T -T_4 >= 0;
R28: +T_4 -A_4*a_4ii -t_RK43 -C_43*d_43 -S_43*x_43 >= 0;
R29: +t_RK43 -t_RK14 -C_14*d_14 -S_14*x_14 -A_4*a_4i >= 0;
R30: +d_14 = +a_4i +a_4ii +d_43;
R31: -d_43 +V*x_43 >= 0;

