<?php

global $graph, $input, $result;

$graph->setup(5);


// Communications
if ($result->value('x_01')) {
	$graph->addCommunication(0, 1, array(0, $input->param('C_01') * $result->value('d_01')));
}

if ($result->value('x_12')) {
	$graph->addCommunication(1, 2, array($result->value('t_RK12'), $result->value('t_RK12') + $input->param('C_12') * $result->value('d_12')));
}

if ($result->value('x_14')) {
	$graph->addCommunication(1, 4, array($result->value('t_RK14'), $result->value('t_RK14') + $input->param('C_14') * $result->value('d_14')));
}

if ($result->value('x_43')) {
	$graph->addCommunication(4, 3, array($result->value('t_RK43'), $result->value('t_RK43') + $input->param('C_43') * $result->value('d_43')));
}

if ($result->value('x_32')) {
	$graph->addCommunication(3, 2, array($result->value('t_RK32'), $result->value('t_RK32') + $input->param('C_32') * $result->value('d_32')));
}

// Services

// P0
$graph->addService(0, array($graph->getCommunicationEnd(0), $graph->getCommunicationEnd(0) + $input->param('A_0') * $result->value('a_0')));

/* @todo similar to 'przypadek1.php' */