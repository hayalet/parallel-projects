<?php

namespace ParallelProcessing;

class InputParser {
	private $data;

	public function __construct($data) {
		$this->data = $data;
	}

	public function param($name) {
		$match = array();

		if (preg_match('/param ' . preg_quote($name) . ' := (\w+)/', $this->data, $match)) {
			return $match[1];
		};

		throw new \InvalidArgumentException("Invalid param: " . $name);
	}

	public function params() {
		$match = array();

		if (preg_match_all('/param (\w+) := (\w+)/', $this->data, $match)) {
			return array_combine($match[1], $match[2]);
		};

		return array();
	}

	public function vars() {
		$match = array();
		if (preg_match_all('/var (\w+)/', $this->data, $match)) {
			return $match[1];
		}

		return array();
	}	
} 

class ResultParser {
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function value($var) {
		$match = array();

		if (preg_match('/'. preg_quote($var) .'\s+(\w+)/', $this->data, $match)) {
			return $match[1];
		};

		throw new \InvalidArgumentException("Invalid variable: " . $var);
	}

	public function values(array $vars) {
		$values = array();
		foreach ($vars as $var) {
			try {
				$values[$var] = $this->value($var);
			} catch(\InvalidArgumentException $e) {}
		}

		return $values;
	}
}

class Graph {

	private $m;

	private $t;

	private $g;

	public function __construct() {
		$this->setup(0);
	}

	public function processors()
	{
		return $this->m;
	}

	public function setup($m) {
		$this->m = $m;

		for ($p = 0; $p < $m; $p++) {
			$this->t[$p] = array();
		}
	}

	public function addCommunication($p1, $p2, array $range) {
		$this->checkProcessor($p1);
		$this->checkProcessor($p2);
		$this->checkRange($range);

		$this->addTask($p1, array(
			'type' => 'communication',
			'direction' => array(
				'from' => $p1,
				'to' => $p2
			),
			'range' => $range
		));

		$this->addTask($p2, array(
			'type' => 'communication',
			'direction' => array(
				'from' => $p2,
				'to' => $p1
			),
			'range' => $range
		));
	}

	public function addService($p, array $range) {
		$this->checkProcessor($p);
		$this->checkRange($range);

		$this->addTask($p, array(
			'type' => 'service',
			'range' => $range
		));
	}

	private function addTask($p, array $task) {
		if ($task['range'][0] == $task['range'][1]) {
			return;
		}

		$this->t[$p][] = $task;

		usort($this->t[$p], function ($t1, $t2) {
			if (($t1['range'][0] < $t2['range'][0] && $t1['range'][1] < $t2['range'][1] && $t2['range'][0] < $t1['range'][1])
				|| ($t1['range'][0] > $t2['range'][0] && $t1['range'][1] > $t2['range'][0] && $t1['range'][0] < $t2['range'][1])
			) {
				throw new \LogicException('Ranges cannot intersect');
			}
			return $t1['range'][0] - $t2['range'][0];
		});
	}

	public function getTasks($p, $type)
	{
		$this->checkProcessor($p);
		$tasks = $this->t[$p];

		$res = array();
		foreach ($tasks as $task) {
			if ($task['type'] == $type) {
				$res[] = $task;
			}
				
		}

		return $res;
	}

	public function getTasksAll()
	{
		$res = array();
			foreach ($this->t as $p => $tasks) {
				foreach ($tasks as $t) {
					$res[] = $t;
				}
			}
			return $res;	
	}

	public function getCommunicationStart($p1, $p2 = null) {
		$tasks = $this->getTasks($p1, 'communication');

		if ($p2 === null) {
			$task = reset($tasks);

			if ($task === false) {
				return 0;
			}

			return $task['range'][0];	
		}

		foreach ($tasks as $task) {
			if ($task['direction']['from'] == $p1 && $task['direction']['to'] == $p2) {
				return $task['range'][0];
			}
		}

		return 0;
	}

	public function getCommunicationEnd($p1, $p2 = null) {
		$tasks = $this->getTasks($p1, 'communication');

		if ($p2 === null) {
			$task = end($tasks);

			if ($task === false) {
				return 0;
			}

			return $task['range'][1];	
		}

		foreach ($tasks as $task) {
			if ($task['direction']['from'] == $p1 && $task['direction']['to'] == $p2) {
				return $task['range'][1];
			}
		}

		return 0;
	}

	public function getServiceStart($p) {
		$tasks = $this->getTasks($p, 'service');

		$task = reset($tasks);

		if ($task === false) {
			return 0;
		}

		return $task['range'][0];	
	}

	public function getServiceEnd($p) {
		$tasks = $this->getTasks($p, 'service');

		$task = end($tasks);

		if ($task === false) {
			return 0;
		}

		return $task['range'][1];	
	}

	public function checkProcessor($p) {
		if ($p >= $this->m || $p < 0) {
			throw new \InvalidArgumentException("Invalid processor number: " . $p);
		}	
	}

	public function checkRange(array $r) {
		if (empty($r) || count($r) != 2) {
			throw new \InvalidArgumentException("Range must contain 2 values, left and right interval side");
		}

		list ($l, $r) = $r;
		if ($l < 0 || $r < 0) {
			throw new \InvalidArgumentException("Range interval cannot contain negative values");
		}

		if ($r < $l) {
			throw new \InvalidArgumentException("Right side of interval is smaller that left");
		}
	}

	public function debug() {
		return var_export($this->t, true);
	}
}

class GanttTexGenerator {
	private $graph;

	private $unit;

	private $min;

	private $max;

	public function __construct(Graph $graph) {
		$this->unit = 0.1;
		$this->graph = $graph;
	}

	public function process() {
		$this->min = null;
		$this->max = null;

		foreach ($this->graph->getTasksAll() as $task) {
			if ($this->min === null || $task['range'][0] < $this->min) {
				$this->min = $task['range'][0];
			}
			if ($this->max === null || $task['range'][1] > $this->max) {
				$this->max = $task['range'][1];
			}
		}

		$this->unit = round(0.056 * $this->max / 266, 3);
	}

	public function generate() {
		$this->process();

$tex = <<<EOD
\begin{ganttchart}[x unit={$this->unit}cm, hgrid, vgrid={*9{draw=none}, dotted}, time slot modifier=0]{{$this->max}}

EOD;

for ($p = 0; $p < $this->graph->processors(); $p++) :

$communications = $this->graph->getTasks($p, 'communication');
$count = count($communications);

foreach ($communications as $i => $task) : 
$from = $task['direction']['from'];
$to = $task['direction']['to'];

if (isset($skip[$from . $to]) || isset($skip[$to . $from])) continue;
else $skip[$from . $to] = true;

$label = '$P_' . $from . '\\rightarrow{}P_' . $to . '$';
$endl = (($i + 1) == $count) ? '\\\\' : '';

$tex .= <<<EOD
\ganttbar[bar/.style={fill=gray, draw=none}]{{$label}}{{$task['range'][0]}}{{$task['range'][1]}} \\\

EOD;
endforeach;

$services = $this->graph->getTasks($p, 'service');
$count = count($services);

foreach ($services as $i => $task) : 
$label = '$P_' . $p . '$';
$endl = (($i + 1) == $count) ? '\\\\' : '';

$tex .= <<<EOD
\ganttbar[bar/.style={fill=black, draw=none}]{{$label}}{{$task['range'][0]}}{{$task['range'][1]}} {$endl}

EOD;
endforeach;

endfor;

$tex .= <<<EOD
\\end{ganttchart}
EOD;

		return $tex;
	}
}

class TableTexGenerator {
	private $data;

	private $chunks;

	public function __construct(array $data, $chunks = 3) {
		$this->data = $data;
		$this->chunks = $chunks;
	}

	public function generate() {

$tex = '';
$divider = count($this->data) / $this->chunks;
$chunks = array_chunk($this->data, ceil($divider), true);
$hspace = round(0.1 / $this->chunks, 2);
$width = round(1 / $this->chunks, 1);

$c = 0;
foreach ($chunks as $data) : 

$tex .= <<<EOD
\begin{tabularx}{{$width}\\textwidth}{ | X | X | }
\hline

EOD;

foreach ($data as $name => $value) :
$name = '$' . preg_replace('/_(.*)$/', '_{\1}', $name) . '$';
$value = '$' . $value . "$";
$tex .= <<<EOD
{$name} & {$value} \\\ \hline

EOD;
endforeach;

$tex .= <<<EOD
\\end{tabularx}

EOD;

if ($c + 1 < count($chunks)) :
$tex .= <<<EOD
\hspace{{$hspace}\\textwidth}

EOD;
endif;

$c++;
endforeach;

		return $tex;
	}
}

?>