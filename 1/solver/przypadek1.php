<?php

global $graph, $input, $result;

$graph->setup(5);

// Communications

// P0
if ($result->value('x_01') * $result->value('d_01')) {
	$graph->addCommunication(0, 1, array(0, $input->param('C_01') * $result->value('d_01')));
}

// P1
if ($result->value('x_12') * $result->value('d_12')) {
	$graph->addCommunication(1, 2, array($result->value('t_RK12'), $result->value('t_RK12') + $input->param('C_12') * $result->value('d_12')));
}

// P2
if ($result->value('x_23') * $result->value('d_23')) {
	$graph->addCommunication(2, 3, array($result->value('t_RK23'), $result->value('t_RK23') + $input->param('C_23') * $result->value('d_23')));
}

// P3
if ($result->value('x_14') * $result->value('d_14')) {
	$graph->addCommunication(1, 4, array($result->value('t_RK14'), $result->value('t_RK14') + $input->param('C_14') * $result->value('d_14')));
}

// P4
if ($result->value('x_43') * $result->value('d_43')) {
	$graph->addCommunication(4, 3, array($result->value('t_RK43'), $result->value('t_RK43') + $input->param('C_43') * $result->value('d_43')));
}

// Services
 
// P0
$graph->addService(0, array($graph->getCommunicationEnd(0), $graph->getCommunicationEnd(0) + $input->param('A_0') * $result->value('a_0')));

// P1
if ($result->value('x_01') * $result->value('d_01')) {
	$start = max($graph->getCommunicationEnd(1, 2), $graph->getCommunicationEnd(1, 4));
	$graph->addService(1, array($start, $start + $input->param('A_1') * $result->value('a_1')));
}

// P2
if ($result->value('x_12') * $result->value('d_12')) {
	if ($result->value('a_2i') && $result->value('a_2ii')) {
		$graph->addService(2, array($graph->getCommunicationEnd(1, 2), $graph->getCommunicationEnd(1, 2) + $input->param('A_2') * $result->value('a_2i')));
		$graph->addService(2, array($result->value('T_2') - $input->param('A_2') * $result->value('a_2ii'), $result->value('T_2')));	
	}
	else if ($result->value('a_2ii')) {
		$graph->addService(2, array($result->value('T_2') - $input->param('A_2') * $result->value('a_2ii'), $result->value('T_2')));	
	}
	else if ($result->value('a_2i')) {
		$graph->addService(2, array($result->value('T_2') - $input->param('A_2') * $result->value('a_2i'), $result->value('T_2')));	
	}
}

// P3
if (($result->value('x_23') * $result->value('d_23')) || ($result->value('x_43') * $result->value('d_43'))) {
	if ($result->value('y_324')) {
		if ($result->value('a_3ii') && $result->value('a_3i')) {
			$graph->addService(3, array($graph->getCommunicationEnd(2, 3), $graph->getCommunicationEnd(2, 3) + $input->param('A_3') * $result->value('a_3i')));
			$graph->addService(3, array($result->value('T_3') - $input->param('A_3') * $result->value('a_3ii'), $result->value('T_3')));
		}
		else if ($result->value('a_3ii')) {
			$graph->addService(3, array($result->value('T_3') - $input->param('A_3') * $result->value('a_3ii'), $result->value('T_3')));
		}
		else if ($result->value('a_3i')) {
			$graph->addService(3, array($result->value('T_3') - $input->param('A_3') * $result->value('a_3i'), $result->value('T_3')));
		}

	} else {
		if ($result->value('a_3ii') && $result->value('a_3i')) {
			$graph->addService(3, array($graph->getCommunicationEnd(4, 3), $graph->getCommunicationEnd(4, 3) + $input->param('A_3') * $result->value('a_3i')));
	  		$graph->addService(3, array($result->value('T_3') - $input->param('A_3') * $result->value('a_3ii'), $result->value('T_3')));
		}
		else if ($result->value('a_3ii')) {
			$graph->addService(3, array($result->value('T_3') - $input->param('A_3') * $result->value('a_3ii'), $result->value('T_3')));
		}
		else if ($result->value('a_3i')) {
			$graph->addService(3, array($result->value('T_3') - $input->param('A_3') * $result->value('a_3i'), $result->value('T_3')));
		}
	}
}

// P4
if ($result->value('x_14') * $result->value('d_14')) {
	if ($result->value('a_4i') && $result->value('a_4ii')) {
		$graph->addService(4, array($graph->getCommunicationEnd(1, 4), $graph->getCommunicationEnd(1, 4) + $input->param('A_4') * $result->value('a_4i')));
		$graph->addService(4, array($result->value('T_4') - $input->param('A_4') * $result->value('a_4ii'), $result->value('T_4')));	
	} 
	else if ($result->value('a_4ii')) {
		$graph->addService(4, array($result->value('T_4') - $input->param('A_4') * $result->value('a_4ii'), $result->value('T_4')));
	}
	else if ($result->value('a_4i')) {
		$graph->addService(4, array($result->value('T_4') - $input->param('A_4') * $result->value('a_4i'), $result->value('T_4')));
	}
}