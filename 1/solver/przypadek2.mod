/**
 * Przypadek 2 - przypadek w którym procesor 1 inicjuje komunikacje
 * do procesorów 2 i 4, kolejność nie jest znana, procesor 2 odbiera
 * komunikacje z procesora 2 oraz procesora 4. Procesor 4 przekazuje
 * dane okrężną drogą do procesora 2 przez ścieżkę 1->3->4->2
 *
 * Przypadek 2 jest analogiczny do przypadku 3, wystarczy zamienić
 * procesory 2 i 4 rolami
 *
 * @author Mateusz Łuczak, Krystian Panek
 * @version 1.0
 */

/**
 * Model Systemu
 */

/* Parametry systemu */
param V := 73;
param M := 10000;

param A_0 := 10;
param A_1 := 5;
param A_2 := 2;
param A_3 := 5;
param A_4 := 10;

param C_01 := 1;
param C_12 := 5;
param C_14 := 3;
param C_32 := 2;
param C_43 := 5;

param S_01 := 0;
param S_12 := 0;
param S_14 := 0;
param S_32 := 0;
param S_43 := 0;

/**
 * Model matematyczny
 */

/* Deklaracje zmiennych */
var T >= 0;
var T_0 >= 0;
var T_1 >= 0;
var T_2 >= 0;
var T_3 >= 0;
var T_4 >= 0;

var t_RK12 >= 0;
var t_RK14 >= 0;
var t_RK32 >= 0;
var t_RK43 >= 0;

var a_0 integer >= 0;
var a_1 integer >= 0;
var a_2 integer >= 0;
var a_2i integer >= 0;
var a_2ii integer >= 0;
var a_3i integer >= 0;
var a_3ii integer >= 0;
var a_4 integer >= 0;

var d_01 integer >= 0;
var d_12 integer >= 0;
var d_14 integer >= 0;
var d_32 integer >= 0;
var d_43 integer >= 0;

var x_01 integer >= 0 <= 1;
var x_12 integer >= 0 <= 1;
var x_14 integer >= 0 <= 1;
var x_32 integer >= 0 <= 1;
var x_43 integer >= 0 <= 1;

var y_124 integer >= 0 <= 1;
var y_213 integer >= 0 <= 1;

/* Funkcja celu */
minimize obj: +T;

/* Ograniczenia */

/*Procesor 0*/
R1: +T -T_0 >= 0;
R2: +T_0 -A_0*a_0 -C_01*d_01 -S_01*x_01 = 0;
R3: +a_0 +d_01 = V;
R4: -d_01 +V*x_01 >= 0;

/* Procesor 1 */
R5: +T -T_1 >= 0;
R6: +T_1 -A_1*a_1 -C_01*d_01 -S_01*x_01 -C_12*d_12 -S_12*x_12 -C_14*d_14 -S_14*x_14 = 0;
R7: +t_RK14 -C_01*d_01 -S_01*x_01 >= 0;
R8: +t_RK12 -C_01*d_01 -S_01*x_01 >= 0;
R9: +t_RK12 -C_01*d_01 -S_01*x_01 -C_14*d_14 -S_14*x_14 +y_124*M >= 0;
R10: +t_RK14 -C_01*d_01 -S_01*x_01 -C_12*d_12 -S_12*x_12 +M -y_124*M >= 0;
R11: -d_14 +V*x_14 >= 0;
R12: -d_12 +V*x_12 >= 0;
R13: +d_01 = +d_12 +d_14 +a_1;

/* Procesor 2 */
R14: +T -T_2 >= 0;
/* Gdy 1->2 jest wcześniej niż 3->2  (y_213 = 1) */
R15: +T_2 -A_2*a_2ii -t_RK32 -C_32*d_32 -S_32*x_32 +M -y_213*M >= 0;
R16: +t_RK32 -t_RK12 -C_12*d_12 -S_12*x_12 -A_2*a_2i +M -y_213*M >= 0;
R17: +d_12 +M -y_213*M >= +a_2i;
/* Gdy 3->2 jest wcześniej niż 1->2  (y_213 = 0) */
R18: +T_2 -A_2*a_2ii -t_RK12 -C_12*d_12 -S_12*x_12 +y_213*M >= 0;
R19: +t_RK12 -t_RK32 -C_32*d_32 -S_32*x_32 -A_2*a_2i +y_213*M >= 0;
R20: +d_32 +y_213*M >= +a_2i;
/* Dla obu */
R21: +d_12 +d_32 = +a_2i +a_2ii;

/* Procesor 3 */
R22: +T -T_3 >= 0;
R23: +T_3 -A_3*a_3ii -t_RK32 -C_32*d_32 -S_32*x_32 = 0;
R24: +t_RK32 -t_RK43 -C_43*d_43 -S_43*x_43 -A_3*a_3i >= 0;
R25: +d_43 = +a_3i +d_32 +a_3ii;
R26: -d_32 +V*x_32 >= 0;

/* Procesor 4 */
R27: +T -T_4 >= 0;
R28: +T_4 -A_4*a_4 -t_RK14 -C_14*d_14 -S_14*x_14 -C_43*d_43 -S_43*x_43 = 0;
R29: +t_RK43 -t_RK14 -C_14*d_14 -S_14*x_14 >= 0;
R30: +d_14 = +a_4 +d_43;
R31: -d_43 +V*x_43 >= 0;

