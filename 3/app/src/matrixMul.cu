/**
 * Copyright 1993-2012 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

/**
 * Matrix multiplication: C = A * B.
 * Host code.
 *
 * This sample implements matrix multiplication as described in Chapter 3
 * of the programming guide.
 * It has been written for clarity of exposition to illustrate various CUDA
 * programming principles, not with the goal of providing the most
 * performant generic kernel for matrix multiplication.
 *
 * See also:
 * V. Volkov and J. Demmel, "Benchmarking GPUs to tune dense linear algebra,"
 * in Proc. 2008 ACM/IEEE Conf. on Superconducting (SC '08),
 * Piscataway, NJ: IEEE Press, 2008, pp. Art. 31:1-11.
 */

#include <stdio.h>
#include <assert.h>
#include <cuda_runtime.h>
#include <helper_functions.h>

/* Version 1 - single thread block */

__global__ void matrixMul_1(float *C, float *A, float *B, int WIDTH) {
    int tx = threadIdx.x;
    int ty = threadIdx.y;
	int r = blockDim.x;

	for(int bx = tx; bx < WIDTH; bx += r)
	{
		for(int by = ty; by < WIDTH; by += r)
		{
			float C_local = 0;
			for(int k = 0; k < WIDTH; ++k)
			{
				float A_elem = A[by * WIDTH + k]; 
				float B_elem = B[k * WIDTH + bx]; 
				C_local += A_elem * B_elem;
			}
			C[by * WIDTH + bx] = C_local;
		}
	}
}

/* Version 2 - grid with multiple thread blocks */

__global__ void matrixMul_2(float *C, float *A, float *B, int WIDTH) {
    int Row = blockIdx.y * blockDim.y + threadIdx.y;
	int Col = blockIdx.x * blockDim.x + threadIdx.x;

	float C_local = 0;
	for(int k = 0; k < WIDTH; ++k)
	{
		C_local += A[Row * WIDTH + k] * B[k * WIDTH + Col];
	}
	C[Row * WIDTH + Col] = C_local;
}

/* Version 3 - grid with multiple thread blocks with shared memory */

template <int BLOCK_SIZE> __global__ void
matrixMul_3(float *C, float *A, float *B, int WIDTH)
{
    int bx = blockIdx.x;
    int by = blockIdx.y;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    int aBegin = WIDTH * BLOCK_SIZE * by;
    int aEnd   = aBegin + WIDTH - 1;
    int aStep  = BLOCK_SIZE;
    int bBegin = BLOCK_SIZE * bx;
    int bStep  = BLOCK_SIZE * WIDTH;

    float Csub = 0;
    for (int a = aBegin, b = bBegin; a <= aEnd; a += aStep, b += bStep)
    {
        __shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
        __shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
        As[ty][tx] = A[a + WIDTH * ty + tx];
        Bs[ty][tx] = B[b + WIDTH * ty + tx];
        __syncthreads();

#pragma unroll
        for (int k = 0; k < BLOCK_SIZE; ++k)
        {
            Csub += As[ty][k] * Bs[k][tx];
        }
        __syncthreads();
    }
    int c = WIDTH * BLOCK_SIZE * by + BLOCK_SIZE * bx;
    C[c + WIDTH * ty + tx] = Csub;
}

/* Version 4 - grid with multiple thread blocks with shared memory */

template <int BLOCK_SIZE> __global__ void
matrixMul_4(float *C, float *A, float *B, int WIDTH)
{
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int aBegin = WIDTH * BLOCK_SIZE * by;
	int aEnd   = aBegin + WIDTH - 1;
	int aStep  = BLOCK_SIZE;
	int bBegin = BLOCK_SIZE * bx;
	int bStep  = BLOCK_SIZE * WIDTH;

	__shared__ float As_read[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float As_write[BLOCK_SIZE][BLOCK_SIZE];
	float (*As_buffer_read)[BLOCK_SIZE];
	float (*As_buffer_write)[BLOCK_SIZE];

	__shared__ float Bs_read[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float Bs_write[BLOCK_SIZE][BLOCK_SIZE];
	float (*Bs_buffer_read)[BLOCK_SIZE];
	float (*Bs_buffer_write)[BLOCK_SIZE];

	As_buffer_write = As_write;
	As_buffer_read = As_read;
	Bs_buffer_write = Bs_write;
	Bs_buffer_read = Bs_read;

	As_buffer_read[ty][tx] = A[aBegin + WIDTH * ty + tx];
	Bs_buffer_read[ty][tx] = B[bBegin + WIDTH * ty + tx];

	__syncthreads();

	float Csub = 0;
	for (int a = aBegin + aStep, b = bBegin + bStep; a <= aEnd; a += aStep, b += bStep)
	{
		As_buffer_write[ty][tx] = A[a + WIDTH * ty + tx];
		Bs_buffer_write[ty][tx] = B[b + WIDTH * ty + tx];

		#pragma unroll
		for (int k = 0; k < BLOCK_SIZE; ++k) {
			Csub += As_buffer_read[ty][k] * Bs_buffer_read[k][tx];
		}

		__syncthreads();

		float (*swap)[BLOCK_SIZE];

		swap = As_buffer_read;
		As_buffer_read = As_buffer_write;
		As_buffer_write = swap;

		swap = Bs_buffer_read;
		Bs_buffer_read = Bs_buffer_write;
		Bs_buffer_write = swap;
	}
	for (int k = 0; k < BLOCK_SIZE; ++k) {
		Csub += As_buffer_read[ty][k] * Bs_buffer_read[k][tx];
	}
	int c = WIDTH * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + WIDTH * ty + tx] = Csub;
}

void constantInit(float *data, int size, float val)
{
    for (int i = 0; i < size; ++i)
    {
        data[i] = val;
    }
}

void setup(int version, int matrix_size, int grid_size, int block_size , dim3& grid, dim3& threads)
{
	switch (version) {
	case 1:
		grid.x = grid.y = grid.z = 1;
		threads.x = threads.y = block_size;
		threads.z = 1;
		break;
	case 2:
		grid.x = grid.y = grid_size;
		grid.z = 1;
		threads.x = threads.y = block_size;
		threads.z = 1;
		break;
	case 3:
		grid.x = grid.y = (matrix_size / block_size);
		grid.z = 1;
		threads.x = threads.y = block_size;
		threads.z = 1;
		break;
	case 4:
		grid.x = grid.y = (matrix_size / block_size);
		grid.z = 1;
		threads.x = threads.y = block_size;
		threads.z = 1;
		break;
	default:
		printf("Version of algorithm should be 1,2,3 or 4");
		break;
	}
}

void runVersion(int version, float* d_C, float* d_A, float* d_B, const dim3& grid, const dim3& threads, int block_size, int matrix_size)
{
	switch (version) {
	case 1:
		matrixMul_1<<< grid, threads >>>(d_C, d_A, d_B, matrix_size);
		break;
	case 2:
		matrixMul_2<<< grid, threads >>>(d_C, d_A, d_B, matrix_size);
		break;
	case 3:
		if (block_size == 16)
		{
			matrixMul_3<16><<< grid, threads >>>(d_C, d_A, d_B, matrix_size);
		}
		else
		{
			matrixMul_3<32><<< grid, threads >>>(d_C, d_A, d_B, matrix_size);
		}
		break;
	case 4:
		if (block_size == 16)
		{
			matrixMul_4<16><<< grid, threads >>>(d_C, d_A, d_B, matrix_size);
		}
		else
		{
			matrixMul_4<32><<< grid, threads >>>(d_C, d_A, d_B, matrix_size);
		}
		break;
	default:
		printf("Version of algorithm should be 1,2,3 or 4");
		break;
	}
}

/**
 * Run a simple test of matrix multiplication using CUDA
 */
int matrixMultiply(int argc, char **argv, int version, int block_size, int grid_size, int matrix_size)
{
    // Allocate host memory for matrices A and B
    unsigned int size_A = matrix_size * matrix_size;
    unsigned int mem_size_A = sizeof(float) * size_A;
    float *h_A = (float *)malloc(mem_size_A);
    unsigned int size_B = matrix_size * matrix_size;
    unsigned int mem_size_B = sizeof(float) * size_B;
    float *h_B = (float *)malloc(mem_size_B);

    // Initialize host memory
    const float valB = 0.01f;
    constantInit(h_A, size_A, 1.0f);
    constantInit(h_B, size_B, valB);

    // Allocate device memory
    float *d_A, *d_B, *d_C;

    // Allocate host matrix C
    unsigned int mem_size_C = matrix_size * matrix_size * sizeof(float);
    float *h_C = (float *) malloc(mem_size_C);

    if (h_C == NULL)
    {
        fprintf(stderr, "Failed to allocate host matrix C!\n");
        exit(EXIT_FAILURE);
    }

    cudaError_t error;

    error = cudaMalloc((void **) &d_A, mem_size_A);

    if (error != cudaSuccess)
    {
        printf("cudaMalloc d_A returned error code %d, line(%d)\n", error, __LINE__);
        exit(EXIT_FAILURE);
    }

    error = cudaMalloc((void **) &d_B, mem_size_B);

    if (error != cudaSuccess)
    {
        printf("cudaMalloc d_B returned error code %d, line(%d)\n", error, __LINE__);
        exit(EXIT_FAILURE);
    }

    error = cudaMalloc((void **) &d_C, mem_size_C);

    if (error != cudaSuccess)
    {
        printf("cudaMalloc d_C returned error code %d, line(%d)\n", error, __LINE__);
        exit(EXIT_FAILURE);
    }

    // copy host memory to device
    error = cudaMemcpy(d_A, h_A, mem_size_A, cudaMemcpyHostToDevice);

    if (error != cudaSuccess)
    {
        printf("cudaMemcpy (d_A,h_A) returned error code %d, line(%d)\n", error, __LINE__);
        exit(EXIT_FAILURE);
    }

    error = cudaMemcpy(d_B, h_B, mem_size_B, cudaMemcpyHostToDevice);

    if (error != cudaSuccess)
    {
        printf("cudaMemcpy (d_B,h_B) returned error code %d, line(%d)\n", error, __LINE__);
        exit(EXIT_FAILURE);
    }

    // Setup execution parameters
    dim3 threads;
    dim3 grid;

	setup(version, matrix_size, grid_size, block_size, grid, threads);

    // Create and start timer
    printf("Computing result using CUDA Kernel...\n");

    // Performs warmup operation using matrixMul CUDA kernel
    runVersion(version, d_C, d_A, d_B, grid, threads, block_size, matrix_size);

    printf("done\n");

    cudaDeviceSynchronize();

    // Allocate CUDA events that we'll use for timing
    cudaEvent_t start;
    error = cudaEventCreate(&start);

    if (error != cudaSuccess)
    {
        fprintf(stderr, "Failed to create start event (error code %s)!\n", cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }

    cudaEvent_t stop;
    error = cudaEventCreate(&stop);

    if (error != cudaSuccess)
    {
        fprintf(stderr, "Failed to create stop event (error code %s)!\n", cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }

    // Record the start event
    error = cudaEventRecord(start, NULL);

    if (error != cudaSuccess)
    {
        fprintf(stderr, "Failed to record start event (error code %s)!\n", cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }

    // Execute the kernel
    int nIter = 10;

    for (int j = 0; j < nIter; j++)
    {
		runVersion(version, d_C, d_A, d_B, grid, threads, block_size, matrix_size);
    }

    // Record the stop event
    error = cudaEventRecord(stop, NULL);

    if (error != cudaSuccess)
    {
        fprintf(stderr, "Failed to record stop event (error code %s)!\n", cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }

    // Wait for the stop event to complete
    error = cudaEventSynchronize(stop);

    if (error != cudaSuccess)
    {
        fprintf(stderr, "Failed to synchronize on the stop event (error code %s)!\n", cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }

    float msecTotal = 0.0f;
    error = cudaEventElapsedTime(&msecTotal, start, stop);

    if (error != cudaSuccess)
    {
        fprintf(stderr, "Failed to get time elapsed between events (error code %s)!\n", cudaGetErrorString(error));
        exit(EXIT_FAILURE);
    }

    // Compute and print the performance
    float msecPerMatrixMul = msecTotal / nIter;
    double flopsPerMatrixMul = 2.0 * (double)matrix_size * (double)matrix_size * (double)matrix_size;
    double gigaFlops = (flopsPerMatrixMul * 1.0e-9f) / (msecPerMatrixMul / 1000.0f);
    printf(
        "Performance= %.2f GFlop/s, Time= %.3f msec, Size= %.0f Ops, WorkgroupSize= %u threads/block\n",
        gigaFlops,
        msecPerMatrixMul,
        flopsPerMatrixMul,
        threads.x * threads.y);

    // Copy result from device to host
    error = cudaMemcpy(h_C, d_C, mem_size_C, cudaMemcpyDeviceToHost);

    if (error != cudaSuccess)
    {
        printf("cudaMemcpy (h_C,d_C) returned error code %d, line(%d)\n", error, __LINE__);
        exit(EXIT_FAILURE);
    }

    printf("Checking computed result for correctness: ");
    bool correct = true;

    for (int i = 0; i < (int)(matrix_size * matrix_size); i++)
    {
        if (fabs(h_C[i] - (matrix_size * valB)) > 1e-2)
        {
            printf("Error! Matrix[%05d]=%.8f, ref=%.8f error term is > 1e-2\n", i, h_C[i], matrix_size*valB);
            correct = false;
        }
    }

    printf("%s\n", correct ? "OK" : "FAIL");

    // Clean up memory
    free(h_A);
    free(h_B);
    free(h_C);
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);

    cudaDeviceReset();

    if (correct)
    {
        return EXIT_SUCCESS;
    }
    else
    {
        return EXIT_FAILURE;
    }
}


/**
 * Program main
 */
int main(int argc, char **argv)
{
    printf("[Matrix Multiply Using CUDA] - Starting...\n");

    if (checkCmdLineFlag(argc, (const char **)argv, "help") ||
        checkCmdLineFlag(argc, (const char **)argv, "?"))
    {
        printf("Usage -device=n (n >= 0 for deviceID)\n");
        printf("      -size=Matrices size\n");
	    printf("      -block=Block size\n");
		printf("      -grid=Grid size\n");
        exit(EXIT_SUCCESS);
    }

    // By default, we use device 0, otherwise we override the device ID based on what is provided at the command line
    int devID = 0;

    if (checkCmdLineFlag(argc, (const char **)argv, "device"))
    {
        devID = getCmdLineArgumentInt(argc, (const char **)argv, "device");
        cudaSetDevice(devID);
    }

    cudaError_t error;
    cudaDeviceProp deviceProp;
    error = cudaGetDevice(&devID);

    if (error != cudaSuccess)
    {
        printf("cudaGetDevice returned error code %d, line(%d)\n", error, __LINE__);
    }

    error = cudaGetDeviceProperties(&deviceProp, devID);

    if (deviceProp.computeMode == cudaComputeModeProhibited)
    {
        fprintf(stderr, "Error: device is running in <Compute Mode Prohibited>, no threads can use ::cudaSetDevice().\n");
        exit(EXIT_SUCCESS);
    }

    if (error != cudaSuccess)
    {
        printf("cudaGetDeviceProperties returned error code %d, line(%d)\n", error, __LINE__);
    }
    else
    {
        printf("GPU Device %d: \"%s\" with compute capability %d.%d\n\n", devID, deviceProp.name, deviceProp.major, deviceProp.minor);
    }

	int matrix_size = 320;

    // size of matrices
    if (checkCmdLineFlag(argc, (const char **)argv, "size"))
    {
        matrix_size = getCmdLineArgumentInt(argc, (const char **)argv, "size");
    }

	int block_size = (deviceProp.major < 2) ? 16 : 32;

	// size of matrices
    if (checkCmdLineFlag(argc, (const char **)argv, "block"))
    {
        block_size = getCmdLineArgumentInt(argc, (const char **)argv, "block");
    }

	int grid_size = 1;

	if (checkCmdLineFlag(argc, (const char **)argv, "grid"))
    {
        grid_size = getCmdLineArgumentInt(argc, (const char **)argv, "grid");
    }

	int version = 1;

	if (checkCmdLineFlag(argc, (const char **)argv, "version"))
    {
        version = getCmdLineArgumentInt(argc, (const char **)argv, "version");
    }


    printf("MatrixA(%d,%d), MatrixB(%d,%d)\n", matrix_size, matrix_size, matrix_size, matrix_size);

    int matrix_result = matrixMultiply(argc, argv, version, block_size, grid_size, matrix_size);

    exit(matrix_result);
}
