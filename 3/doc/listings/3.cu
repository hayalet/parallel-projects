__global__ void matrixMul_3(float *C, float *A, float *B, int WIDTH) {
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int aBegin = WIDTH * BLOCK_SIZE * by;
	int aEnd   = aBegin + WIDTH - 1;
	int aStep  = BLOCK_SIZE;
	int bBegin = BLOCK_SIZE * bx;
	int bStep  = BLOCK_SIZE * WIDTH;

	float Csub = 0;
	for (int a = aBegin, b = bBegin; a <= aEnd; a += aStep, b += bStep)	{
		__shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];
		As[ty][tx] = A[a + WIDTH * ty + tx];
		Bs[ty][tx] = B[b + WIDTH * ty + tx];
		__syncthreads();
	#pragma unroll
		for (int k = 0; k < BLOCK_SIZE; ++k) {
			Csub += As[ty][k] * Bs[k][tx];
		}
		__syncthreads();
	}
	int c = WIDTH * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + WIDTH * ty + tx] = Csub;
}