__global__ void matrixMul_1(float *C, float *A, float *B, int WIDTH) {
	int tx = threadIdx.x;
	int ty = threadIdx.y;
	int r = blockDim.x;

	for(int bx = tx; bx < WIDTH; bx += r) {
		for(int by = ty; by < WIDTH; by += r) {
			float C_local = 0;
			for(int k = 0; k < WIDTH; ++k) {
				float A_elem = A[by * WIDTH + k]; 
				float B_elem = B[k * WIDTH + bx]; 
				C_local += A_elem * B_elem;
			}
			C[by * WIDTH + bx] = C_local;
		}
	}
}