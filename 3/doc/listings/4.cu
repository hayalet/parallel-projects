__global__ void matrixMul_4(float *C, float *A, float *B, int WIDTH) {
	int bx = blockIdx.x;
	int by = blockIdx.y;
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	int aBegin = WIDTH * BLOCK_SIZE * by;
	int aEnd   = aBegin + WIDTH - 1;
	int aStep  = BLOCK_SIZE;
	int bBegin = BLOCK_SIZE * bx;
	int bStep  = BLOCK_SIZE * WIDTH;

	__shared__ float As_read[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float As_write[BLOCK_SIZE][BLOCK_SIZE];
	float (*As_buffer_read)[BLOCK_SIZE];
	float (*As_buffer_write)[BLOCK_SIZE];

	__shared__ float Bs_read[BLOCK_SIZE][BLOCK_SIZE];
	__shared__ float Bs_write[BLOCK_SIZE][BLOCK_SIZE];
	float (*Bs_buffer_read)[BLOCK_SIZE];
	float (*Bs_buffer_write)[BLOCK_SIZE];

	As_buffer_write = As_write;
	As_buffer_read = As_read;
	Bs_buffer_write = Bs_write;
	Bs_buffer_read = Bs_read;

	As_buffer_read[ty][tx] = A[aBegin + WIDTH * ty + tx];
	Bs_buffer_read[ty][tx] = B[bBegin + WIDTH * ty + tx];

	__syncthreads();

	float Csub = 0;
	for (int a = aBegin + aStep, b = bBegin + bStep; a <= aEnd; a += aStep, 
			 b += bStep) {
		As_buffer_write[ty][tx] = A[a + WIDTH * ty + tx];
		Bs_buffer_write[ty][tx] = B[b + WIDTH * ty + tx];

		#pragma unroll
		for (int k = 0; k < BLOCK_SIZE; ++k) {
			Csub += As_buffer_read[ty][k] * Bs_buffer_read[k][tx];
		}

		__syncthreads();

		float (*swap)[BLOCK_SIZE];

		swap = As_buffer_read;
		As_buffer_read = As_buffer_write;
		As_buffer_write = swap;

		swap = Bs_buffer_read;
		Bs_buffer_read = Bs_buffer_write;
		Bs_buffer_write = swap;
	}
	for (int k = 0; k < BLOCK_SIZE; ++k) {
		Csub += As_buffer_read[ty][k] * Bs_buffer_read[k][tx];
	}
	int c = WIDTH * BLOCK_SIZE * by + BLOCK_SIZE * bx;
	C[c + WIDTH * ty + tx] = Csub;
}
