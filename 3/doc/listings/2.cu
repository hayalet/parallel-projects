__global__ void matrixMul_2(float *C, float *A, float *B, int WIDTH) {
	int Row = blockIdx.y * blockDim.y + threadIdx.y;
	int Col = blockIdx.x * blockDim.x + threadIdx.x;

	float C_local = 0;
	for(int k = 0; k < WIDTH; ++k) {
		C_local += A[Row * WIDTH + k] * B[k * WIDTH + Col];
	}
	C[Row * WIDTH + Col] = C_local;
}