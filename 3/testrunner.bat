@echo off
set "ver=4" 
set "block_size=16"
set "filename=%ver%_%block_size%_raw.dat"
cd matrixMul\bin
.\matrixMul.exe -version=%ver% -size=128 -block=%block_size% >> ..\..\data\%filename%
.\matrixMul.exe -version=%ver% -size=256 -block=%block_size% >> ..\..\data\%filename%
.\matrixMul.exe -version=%ver% -size=512 -block=%block_size% >> ..\..\data\%filename%
.\matrixMul.exe -version=%ver% -size=1024 -block=%block_size% >> ..\..\data\%filename%
.\matrixMul.exe -version=%ver% -size=2048 -block=%block_size% >> ..\..\data\%filename%
.\matrixMul.exe -version=%ver% -size=3072 -block=%block_size% >> ..\..\data\%filename%
exit