#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char * argv[]){

	/* DECLARING VARIABLES */

	const int N = atoi(argv[1]);

	clock_t start, stop;

	float ** matrix_a = new float * [N];
	float ** matrix_b = new float * [N];
	float ** matrix_c = new float * [N];

	/* FILLING MATRICES WITH RANDOM NUMBERS */

	for (int i = 0; i < N; i++){
		matrix_a[i] = new float[N];
		matrix_b[i] = new float[N];
		matrix_c[i] = new float[N];
		for (int j = 0; j < N; j++){
			matrix_a[i][j] = (float)(rand() / RAND_MAX);
			matrix_b[i][j] = (float)(rand() / RAND_MAX);
			matrix_c[i][j] = 0.0f;
		}
	}

	/* MATRICES MULTIPLICATION */

	start = clock();

	for (int i = 0; i < N; i++){
		for (int k = 0; k < N; k++){
			for (int j = 0; j < N; j++){
				matrix_c[i][j] += matrix_a[i][k] * matrix_b[k][j];
			}	
		}
	}

	stop = clock();

	/* OUTPUT */ 

	printf("Completed in time %f s\n",((double) (stop - start)/1000000.0));

	/* MEMORY CLEAN UP */

	for (int i = 0 ; i < N; i++) {
		delete matrix_a[i];
		delete matrix_b[i];
		delete matrix_c[i];
	}

	delete matrix_a;
	delete matrix_b;
	delete matrix_c;

	return 0;
}