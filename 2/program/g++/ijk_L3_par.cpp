#include <omp.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>

#define L3_CACHE_SIZE 8192 //kB

using namespace std;

int main(int argc, char * argv[]){

	/* DECLARING VARIABLES */

	const int N = atoi(argv[1]);
	double start, stop;

	float ** matrix_a = new float * [N];
	float ** matrix_b = new float * [N];
	float ** matrix_c = new float * [N];

	/* FILLING MATRICES WITH RANDOM NUMBERS */

	for (int i = 0; i < N; i++){
		matrix_a[i] = new float[N];
		matrix_b[i] = new float[N];
		matrix_c[i] = new float[N];
		for (int j = 0; j < N; j++){
			matrix_a[i][j] = (float)(rand() / RAND_MAX);
			matrix_b[i][j] = (float)(rand() / RAND_MAX);
			matrix_c[i][j] = 0.0f;
		}
	}

	/* CALCULATING TILE_SIZE */

	const int TILE_SIZE = sqrt((L3_CACHE_SIZE * 1024 / sizeof(float)) / 3);

	/* MATRICES MULTIPLICATION */

	start = omp_get_wtime();
	
	#pragma omp parallel for
	for (int i = 0; i < N; i += TILE_SIZE) {
		for (int j = 0; j < N; j += TILE_SIZE) {
			for (int k = 0; k < N; k += TILE_SIZE) {
				for (int ii = i; ii < min(i + TILE_SIZE - 1,N); ii++) {
					for (int jj = j; jj < min(j + TILE_SIZE - 1,N); jj++) {
						for (int kk = k; kk < min(k + TILE_SIZE - 1,N); kk++) {
							matrix_c[ii][jj] += matrix_a[ii][kk] * matrix_b[kk][jj];
						}
					}
				}
			}	
		}
	}

	stop = omp_get_wtime();

	/* OUTPUT */

	printf("Completed in time %f s\n",stop - start);

	/* MEMORY CLEAN UP */

	for (int i = 0 ; i < N; i++) {
		delete matrix_a[i];
		delete matrix_b[i];
		delete matrix_c[i];
	}

	delete matrix_a;
	delete matrix_b;
	delete matrix_c;

	return 0;
}