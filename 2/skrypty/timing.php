<?php

if ($argc != 5) {
    printf("Invalid arguments, provide: [dir] [executables] [instances] [targetFile]" . PHP_EOL);
    exit(1);
}

if (!is_dir($argv[1])) {
    printf("Directory does not exist" . PHP_EOL);
    exit(1);
}

$dir = realpath($argv[1]);
$files = explode(',', $argv[2]);
$instances = explode(',', $argv[3]);
$target = $argv[4];

$results = array();

foreach ($instances as $instance) {
    foreach ($files as $file) {
        $path = $dir . DIRECTORY_SEPARATOR . $file;

        if (!file_exists($path)) {
            printf('Could not find file: %s' . PHP_EOL, $path);
            exit(1);
        }

        $result = null;

        printf('Executing: %s %s' . PHP_EOL, $file, $instance);

        $start = microtime(true);
        exec($path . ' ' . $instance, $result);
        $stop = microtime(true);

        printf('Elapsed time: %f [ms]' . PHP_EOL, $stop - $start);

        $result = implode(PHP_EOL, $result);

        if (!isset($results[$file])) {
            $results[$file] = array();
        }

        $results[$file][$instance] = $result;
    }
}

printf("Saving to: %s" . PHP_EOL, $target);

$csv = 'instance;' . implode(';', $files) . ';' . PHP_EOL;
foreach ($instances as $instance) {
    $csv .= $instance . ';';

    foreach ($files as $file) {
        $csv .= $results[$file][$instance] . ';';
    }

    $csv .= PHP_EOL;
}

echo $csv;

if (!file_put_contents($target, $csv)) {
    printf('Could not save result' . PHP_EOL);
}

exit(0);



