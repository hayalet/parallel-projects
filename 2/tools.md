# Użyteczne narzędzia pod Linuxem

## Sprawdzenie danych procka

### Na każdym systemie

`cat /proc/cpuinfo`

`cat /sys/devices/system/cpu/cpu?/cache/index?/*`

- `cpu?` - rdzeń procka
- `index?` - poziom cache

### Przy użyciu aplikacji (coś jak cpu-z)

`hardinfo`

## Kompilacja

`g++ -g -O3 -march=corei7 -fopenmp a.cpp -o out/a.o`

- `-g` - generowanie informacji, które wykorzystują `gdb` czy `valgrind`
- `-O3` - ,,agresywna'' optymalizacja (z wektoryzacją pętli itd)
- `-march=*` - optymalizacja pod konkretny procesor

## Pomiar czasu wykonania

`time a.out`

## Profilowanie cache

### Perf

`perf stat --repeat 5 -e cycles:u -e instructions:u -e l1-dcache-loads:u -e l1-dcache-load-misses:u -e llc-loads:u -e llc-load-misses:u -e dTLB-loads:u -e dTLB-load-misses:u ./out/a.o`

Oznaczenia:

- `l1-dcache` - L1 data cache

- `llc` - Last Level cache (cache L3)

- `dTLB` - Dual Translation Lookaside Buffer

### Valgrind

`valgrind --tool=callgrind --cache-sim=yes ./out/a.o args`

Raport

`callgrind_annotate callgrind.out.pid`

Raport linia po lini

`callgrind_annotate callgrind.out.pid a.cpp`

Oznaczenia w raportach:

- `Ir` - I cache reads (number of instructions executed)

- `Dr` - D cache reads (number of memory reads)

- `Dw` - D cache writes (number of memory writes)

- `I1mr` - I1 cache read misses

- `D1mr` - D1 cache read misses

- `D1mw` - D1 cache write misses

- `ILmr` - LL cache instruction read misses

- `DLmr` - LL cache data read misses

- `DLmw` - LL cache data write misses