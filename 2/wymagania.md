# Przetwarzanie równoległe PROJEKT 2

Temat projektu dotyczy przetwarzania równoległego realizowanego w komputerze równoległym z procesorem 
wielordzeniowym z pamięcią współdzieloną (komputer  z procesorem 4 rdzeniowym AMD jest dostępny w 
laboratorium 2.7.6). Projekt polega na napisaniu prostego programu realizującego obliczenia dla jednego z 
opisanych poniżej tematów projektu (temat dla grupy określa prowadzący zajęcia). W ramach ćwiczenia 
wykorzystywany jest CodeAnalyst – do pomiaru częstości występowania zdarzeń procesora wpływających na 
efektywność przetwarzania i wyznaczania miar efektywności systemu. 

## ZAKRES: 
Badanie przygotowanego kodu (wersji kodów) będzie uwzględniać następujące elementy: 
* wyznaczenie najlepszego czasu przetwarzania dla obliczeń sekwencyjnych – czasu będącego punktem 
obliczeń  przyspieszenia i efektywność w funkcji liczby procesorów, 
* sposoby szeregowania iteracji pętli, 
* badanie podejść różniących się lokalnością dostępu do pamięci  i zrównoważeniem obciążenia 
procesorów (badanie niezrównoważenia za pomocą programu profilującego), 
* do obliczeń proszę wykorzystać tablice zainicjowane liczbami losowymi  (np. matrix_a[i][j] = 
(float) rand() / RAND_MAX ;)
* wyznaczenie miar efektywności przetwarzania.  Propozycja wyznaczanych miar efektywności 
przetwarzania dla poszczególnych procesorów: IPC, stosunki braku trafień do poszczególnych poziomów 
pp (w funkcji  rodzaju braku trafienia), stosunki braku trafień do DTLB, ilość danych pobranych z RAM, 
czasy obsługi braków trafień do pp i DTLB 
* eksperymenty należy dokonać dla instancji charakterystycznych pod względem zapotrzebowania na 
pamięć  odpowiadającego wielkości dostępnej dla poszczególnych rdzeni pamięci podręcznej w 
poszczególnych poziomach pamięci. 
Sprawozdanie zawierać powinno: 
* krótki opis problemu i zastosowanego algorytmu, 
* opis eksperymentu obliczeniowego i jego cele, 
* wyniki eksperymentów z analizą, miary efektywności proszę podać wraz z wzorami służącymi do ich 
wyznaczenia,  
* wnioski z uzyskanych wyników eksperymentów przedstawione w punktach. 

 Wymagana jest wersja sprawozdania drukowana (dwustronnie)  i elektroniczna,  sprawozdania ma zawierać
skomentowane,  kluczowe dla zrozumienia tematu fragmenty testowanego kodu, wersja elektroniczna 
dokumentacji zawiera pliki źródłowe kodu. 

## Tematy projektu: 
5. Mnożenie macierzy porównanie efektywności metod –  
* 3 pętle kolejność pętli: ikj,  
* 3 pętle kolejność pętli: ijk,  
* 6 pętli kolejność pętli: ijk optymalizowane do wielkości poziomów pp, współdzielenie danych w 
pp L3. 